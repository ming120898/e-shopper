<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">
                        Thêm sản phẩm
                    </header>
                    <div class="panel-body">
                        <form role="form" action="/e-shopper/admin/addNewProd" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên sản phẩm</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="prod_name" value="${pro_name}">
                                <c:choose>
                                    <c:when test="${proNamVali==1}">
                                        <div class="error" style="color: red">Vui lòng nhập tên sản phẩm.</div>
                                    </c:when>
                                    <c:when test="${proNamVali==2}">
                                        <div class="error" style="color: red">Tên sản phẩm dài ít nhất 6 kí tự.</div>
                                    </c:when>
                                </c:choose>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Giá</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="prod_price"value="${pro_price}">
                                 <c:choose>
                                    <c:when test="${proPricVali==1}">
                                        <div class="error" style="color: red">Vui lòng nhập Giá sản phẩm.</div>
                                    </c:when>
                                    <c:when test="${proPricVali==2}">
                                        <div class="error" style="color: red">Giá sản phẩm phải là 1 số.</div>
                                    </c:when>
                                 </c:choose>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Số lượng trong kho</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="prod_stock" value="${pro_stock}">
                                 <c:choose>
                                    <c:when test="${proStoVali==1}">
                                        <div class="error" style="color: red">Vui lòng nhập số lượng trong kho.</div>
                                    </c:when>
                                    <c:when test="${proStoVali==2}">
                                        <div class="error" style="color: red">Số lượng phải là 1 số.</div>
                                    </c:when>
                                 </c:choose>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mô tả</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="prod_details" value="${pro_details}">
                                <c:choose>
                                    <c:when test="${proNamVali==1}">
                                        <div class="error" style="color: red">Vui lòng nhập Tên sản phẩm.</div>
                                    </c:when>
                                    <c:when test="${proNamVali==2}">
                                        <div class="error" style="color: red"> Tên sản phẩm dài ít nhất 6 kí tự.</div>
                                    </c:when>
                                </c:choose>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Ảnh sản phẩm</label>
                                <input type="file" name="prod_img_link"/>
                                 <c:choose>
                                    <c:when test="${proImgLinkVali==1}">
                                        <div class="error" style="color: red">Vui lòng chọn Hình ảnh.</div>
                                    </c:when>
                                    <c:when test="${proImgLinkVali==2}">
                                        <div class="error" style="color: red">Vui lòng chọn chính xác địa chỉ hình ảnh</div>
                                    </c:when>
                                 </c:choose>
                                        
                            </div>

                            <button type="submit" class="btn btn-info">Thêm sản phẩm</button>
                        </form>

                    </div>
                </section>
            </div>
        </div>          
    </section>
</aside>
