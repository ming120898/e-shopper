<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section class="main-content">				
    <div class="row">
        <div class="span12">					
            <h4 class="title"><span class="text"><strong>Login Form </strong>cho Quản trị viên</span></h4>
            <form action="/e-shopper/admin/signinAd" method="post">
                <input type="hidden" name="next" value="/">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input type="text" required=""name="ad_name" placeholder="Enter your name here" id="username" class="input-xlarge">

                        </div>
                        
                    </div>
                    <div class="control-group">
                        <label class="control-label">Password</label>
                        <div class="controls">
                            <input type="password" required="" name="ad_pass" placeholder="Enter your password here" id="password" class="input-xlarge">

                        </div>
                       
                    </div>
                    <div class="control-group">
                        <input tabindex="3" class="btn btn-inverse large" type="submit" value="Sign in">
                        <hr>
                        <p class="reset">Khôi phục<a tabindex="4" href="#" title="Recover your username or password"> Tài khoản</a></p>
                    </div>
                </fieldset>
            </form>				
        </div>

    </div>
</section>	