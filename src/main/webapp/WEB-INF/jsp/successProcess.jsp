<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<aside class="right-side">
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <c:choose>
                         <c:when test="${succAddProd==1}">
                            <center>
                                <label class="control-label" style="font-size: 16px;color: green">Thêm sản phẩm thành công.</label>
                            </center>
                        </c:when>
                        <c:when test="${succDel==1}">
                            <center>
                                <label class="control-label" style="font-size: 16px;color: green">Xóa sản phẩm thành công.</label>
                            </center>
                        </c:when>
						<c:when test="${succEdit==1}">
                            <center>
                                <label class="control-label" style="font-size: 16px;color: green">Sản phẩm đã được cập nhật.</label>
                            </center>
                        </c:when>
						<c:when test="${succConf ==1}">
                            <center>
                                <label class="control-label" style="font-size: 16px;color: green">Đã xác nhận.</label>
                            </center>
                        </c:when>
						<c:when test="${succDecl==1}">
                            <center>
                                <label class="control-label" style="font-size: 16px;color: green">Đã hủy đơn hàng.</label>
                            </center>
                        </c:when>
                    </c:choose>
                </div>
            </div>
        </div>
    </section>
</aside>
