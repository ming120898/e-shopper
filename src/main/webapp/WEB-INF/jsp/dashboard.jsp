<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<aside class="right-side">
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">

            <div class="col-xs-12">
                <!--earning graph start-->
                <div class="panel">
                    <header class="panel-heading">
                        Transactions
                    </header>
                    <div class="panel-body table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>STT</th>        
                                <th>ID Đơn Hàng</th>
                                <th>ID Khách Hàng</th>
                                <th>Tên khách hàng</th>
                                <th>Số điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Số Lượng</th>
                                <th>Ngày Tạo</th>
                                <th>ID Sản Phẩm</th>
                                <th>Tên Sản Phẩm</th>
                                <th>Hình</th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                    <td>1</td>
                                    <td>0101</td>
                                    <td>00111</td>
                                    <td>Bùi Văn Nghĩa</td>
                                    <td>0965424551</td>
                                    <td>Quy Nhơn</td>
                                    <td>2</td>
                                    <td>10/10/2019</td>
                                    <td>abc</td>
                                    <td>Quần jean</td>
                                    <td>
										<a href="">
                                            <img alt="" src="">
                                        </a>
                                    <td/>                                                             
                                </tr>
                        </table>  
                    </div>
                </div>
                <!--earning graph end-->
        </div>
        
        </div>
    </section>
</aside>
<script>
    $('#noti-box').slimScroll({
        height: '400px',
        size: '5px',
        BorderRadius: '5px'
    });

    $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
        checkboxClass: 'icheckbox_flat-grey',
        radioClass: 'iradio_flat-grey'
    });
</script>
