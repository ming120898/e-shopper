<%-- 
    Document   : cart
    Created on : Jul 6, 2017, 11:18:35 PM
    Author     : User
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section class="main-content">				
    <div class="row">
        <div class="span9" style = "margin-left: 130px;">	
            <form class="form-inline" method="post" action="/e-shopper/cart">
                <h4 class="title"><span class="text"><strong>Giỏ Hàng của bán</strong></span></h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Xóa</th>
                            <th>Hình Sản Phẩm</th>
                            <th>Tên Sản Phẩm</th>
                            <th>Số Lượng</th>
                            <th>Giá</th>
                            <th>Tổng</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${cart}" var="item">
                            <tr>
                                <td>
                                    <input type="checkbox" value="${item.prod.pro_id}" name="remove">
                                </td>
                                <td><a href="productDetail?id=${item.prod.pro_id}"><img alt="" src="${item.prod.pro_img_link}"></a></td>
                                <td>${item.prod.pro_name}</td>
                                <td><input type="text" name="quantity" class="span1" value="${item.quantity}" required="" ></td>
                                <td>${item.prod.pro_price} VND</td>
                                <td>${item.getAmount()} VND</td>

                            </tr>
                        </c:forEach>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><strong style="color:red">Thành Tiền</strong></td>
                            <td><strong>${amountTotal} VND</strong></td>
                        </tr>		  
                    </tbody>
                </table>
                
                <hr>
                <p class="cart-total right">
                    <strong>Tạm Tính</strong>: $${amountTotal}<br>
                    <strong>Thuế (-0.00)</strong>: 0.00 VND<br>
                    <strong>VAT (0.00%)</strong>: 0.00 VND<br>
                    <strong style="color:red">THÀNH TIỀN</strong>: ${amountTotal} VND<br>
                </p>
                <hr/>
                <input type="checkbox" checked="checked" name="remove" value="-100" style="visibility: hidden;"/>
                <p class="buttons center">				
                    <button class="btn" type="submit">Cập Nhật</button>
                    <button class="btn" type="button"><a style="color: #333" href="/e-shopper/">Tiếp tục mua hàng</a></button>
                    <button class="btn btn-inverse" type="button" id="checkout">
                        <a style="color: #fff"href="/e-shopper/checkout">Tiến hành đặt hàng</a>
                    </button>
                </p>			
            </form>
        </div>
    </div>
</section>	
