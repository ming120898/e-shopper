<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section class="header_text">
</section>
<section class="main-content">
    <div class="row">
        <div class="span12">								
            <ul class="thumbnails listing-products">
                <c:forEach items="${proList}" var="prod" end="12" begin="0">
                    <li class="span3">
                        <div class="product-box">												
                            <a href="productDetail?id=${prod.pro_id}"><img alt="" src="${prod.pro_img_link}"></a><br/>
                            <a href="productDetail?id=${prod.pro_id}" class="title">${prod.pro_name}</a><br/>
                            <a href="#" class="category">${prod.pro_category}</a><br/>
                            <p class="price">${prod.pro_price} VND</p>
                        </div>
                    </li>
                </c:forEach>

            </ul>								
            <hr>
        </div>
        
    </div>
</section>



