<%-- 
    Document   : checkout
    Created on : Jul 6, 2017, 9:07:35 PM
    Author     : User
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section class="main-content">
    <div class="row">
        <div class="span12">
            <div class="accordion" id="accordion2">

                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="">Tài Khoản &amp; Chi tiết đặt hàng</a>
                    </div>
                    <div id="collapseTwo" class="accordion-body ">
                        <div class="accordion-inner">
                            <div class="row-fluid">
                                <div class="span6" style="">
                                    <h4>Your Personal Details</h4>
                                    <form method="post" action="/e-shopper/checkout">
                                        <div class="control-group">
                                            <label class="control-label"><span class="required">*</span><strong>Tên</strong></label>
                                            <div class="controls">
                                                <input type="text" placeholder="" required=""class="input-xlarge" name="name" 
                                                       value="${name}">
                                            </div>
                                            <c:choose>
                                                <c:when test="${nameVali==1}">
                                                    <div class="error" style="color: red">Vui lòng nhập đúng tên.</div>
                                                </c:when>
                                                <c:when test="${nameVali==2}">
                                                    <div class="error" style="color: red">Tên ít nhất phải từ 6 ký tự.</div>
                                                </c:when>
                                            </c:choose>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"><span class="required">*</span><strong>Email Address</strong></label>
                                            <div class="controls">
                                                <input type="text" placeholder="" required="" class="input-xlarge" name="email" 
                                                       value="${email}" >
                                            </div>
                                            <c:choose>
                                                <c:when test="${emailVali==1}">
                                                    <div class="error"style="color: red">Vui lòng nhập chính xác địa chỉ email của bạn.</div>
                                                </c:when>
                                                <c:when test="${emailVali == 2}">
                                                    <div class="error"style="color: red">Địa chỉ bạn nhập vào chưa đúng định dạng email.</div>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span class="required">*</span><strong>Điện thoại</strong></label>
                                            <div class="controls">
                                                <input type="text" placeholder="" required=""class="input-xlarge" name="mobile"
                                                       value="${mobile}">
                                            </div>
                                            <c:choose>
                                                <c:when test="${mobileVali == 1}">
                                                    <div class="error"style="color: red">Vui lòng nhập chính xác Số điện thoại của bạn.</div>
                                                </c:when>
                                                <c:when test="${mobileVali == 2}">
                                                    <div class="error"style="color: red">Số điện thoại phải từ 8 số trở lên.</div>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"> <span class="required">*</span><strong>Thành Phố</strong></label>
                                            <div class="controls">
                                                <select name="province" class="input-xlarge">
                                                    <option value=""> --- Vui lòng chọn Thành Phố --- </option>
                                                    <option value="Ho Chi Minh">Ho Chi Minh</option>
                                                    <option value="Ha Noi">Ha Noi</option>
                                                    <option value="Da Nang">Da Nang</option>
                                                    <option value="Quy Nhon">Quy Nhon</option>
                                                </select>
                                            </div>
                                            <c:choose>
                                                <c:when test="${province eq ''}">
                                                    <div class="error"style="color: red">Chọn Quận/Huyện/Phường.</div>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span class="required">*</span> <strong>Quận/Huyện</strong></label>
                                            <div class="controls">
                                                <select name="district" class="input-xlarge">
                                                    <option value=""> --- Vui lòng chọn--- </option>
                                                    <option value="Quan a">Quận 1</option>
                                                    <option value="Hoan Kiem">Hoàn Kiếm</option>
                                                    <option value="Son Tra">Sơn Trà</option>
                                                    <option value="Ngo May">Ngô Mây</option>
                                                </select>
                                            </div>
                                            <c:choose>
                                                <c:when test="${district eq ''}">
                                                    <div class="error"style="color: red">Vui lòng chọn.</div>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span class="required">*</span><strong>Địa chỉ</strong></label>
                                            <div class="controls">
                                                <input type="text" required="" placeholder="" class="input-xlarge" name="address"
                                                       value="${address}">
                                            </div>
                                            <c:choose>
                                                <c:when test="${addressVali == 1}">
                                                    <div class="error"style="color: red">Vui lòng chọn địa chỉ.</div>
                                                </c:when>
                                                <c:when test="${addressVali ==2}">
                                                    <div class="error"style="color: red">Địa chỉ ít nhất 8 kí tự.</div>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                        <button class="btn btn-inverse pull-left" type="submit">Đặt Hàng</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>				
        </div>
    </div>
</section>			
