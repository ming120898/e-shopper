<%-- 
    Document   : productDetail
    Created on : Jul 6, 2017, 8:55:36 PM
    Author     : User
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.e_shopper.beans.product"%>
<section class="main-content">				
    <div class="row">						
        <div class="span9">
            <div class="row">
                <div class="span4">
                    <a href="${prod.pro_img_link}" class="thumbnail" data-fancybox-group="group1" title="Description 1"><img alt="" src="${prod.pro_img_link}"></a>												
                    <ul class="thumbnails small">								
                        <li class="span1">
                            <a href="/e-shopper/assets/themes/images/ladies/2.jpg" class="thumbnail" data-fancybox-group="group1" title="Description 2"><img src="/e-shopper/assets/themes/images/ladies/2.jpg" alt=""></a>
                        </li>								
                        <li class="span1">
                            <a href="/e-shopper/assets/themes/images/ladies/3.jpg" class="thumbnail" data-fancybox-group="group1" title="Description 3"><img src="/e-shopper/assets/themes/images/ladies/3.jpg" alt=""></a>
                        </li>													
                        <li class="span1">
                            <a href="/e-shopper/assets/themes/images/ladies/4.jpg" class="thumbnail" data-fancybox-group="group1" title="Description 4"><img src="/e-shopper/assets/themes/images/ladies/4.jpg" alt=""></a>
                        </li>
                        <li class="span1">
                            <a href="/e-shopper/assets/themes/images/ladies/5.jpg" class="thumbnail" data-fancybox-group="group1" title="Description 5"><img src="/e-shopper/assets/themes/images/ladies/5.jpg" alt=""></a>
                        </li>
                    </ul>
                </div>
                <div class="span5">
                    <address>
                        <strong>Mã sản phẩm:</strong> <span>${prod.pro_id}</span><br>
                        <strong>Tên sản phẩm:</strong> <span style ="color:red">${prod.pro_name}</span><br>
                        <strong>Số lượng còn :</strong> <span>${prod.pro_stock}</span><br>								
                    </address>									
                    <h4><strong>Giá: ${prod.pro_price} VND</strong></h4>
                </div>
                <div class="span5">
                    <form class="form-inline" method="post" action="/e-shopper/cart?prod_id=${prod.pro_id}">
                        <label>Số lượng:</label>
                        <input type="text" name="quantity" class="span1" value="1" required="" >
                        <button class="btn btn-inverse" type="submit">Mua ngay</button>
                    </form>
                    
                </div>							
            </div>
            <div class="row">
                <div class="span9">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active"><a href="#home">Mô Tả</a></li>
                        <li class=""><a href="#profile">Thông tin bổ sung</a></li>
                    </ul>							 
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">${prod.pro_details}</div>
                        <div class="tab-pane" id="profile">
                            <table class="table table-striped shop_attributes">	
                                <tbody>
                                    <tr class="">
                                        <th>Size</th>
                                        <td>Large, Medium, Small, X-Large</td>
                                    </tr>		
                                    <tr class="alt">
                                        <th>Colour</th>
                                        <td>Orange, Yellow</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>							
                </div>						
                
        </div>
       
        </div>
    </div>
</section>			

<script>
    $(function () {
        $('#myTab a:first').tab('show');
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })
    })
    $(document).ready(function () {
        $('.thumbnail').fancybox({
            openEffect: 'none',
            closeEffect: 'none'
        });

        $('#myCarousel-2').carousel({
            interval: 2500
        });
    });
</script>
