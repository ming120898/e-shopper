-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 07, 2020 lúc 02:00 PM
-- Phiên bản máy phục vụ: 10.4.8-MariaDB
-- Phiên bản PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `e-shopper`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) NOT NULL,
  `admin_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_pass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_pass`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `custom_id` int(10) NOT NULL,
  `custom_pass` varchar(50) NOT NULL,
  `custom_email` varchar(60) NOT NULL,
  `custom_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_phone` varchar(15) NOT NULL,
  `custom_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `custom_province` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `custom_district` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`custom_id`, `custom_pass`, `custom_email`, `custom_name`, `custom_phone`, `custom_address`, `custom_province`, `custom_district`) VALUES
(52, '', 'zbc@gmail.com', 'Phan Nhật Minh', '3434343545', '23 Hoàn Kiem', 'Hai Phong', 'Hoang Mai'),
(53, '', 'abc@gmail.com', 'Nguyễn Văn A', '534543545', '34  nguyễn chánh', 'Ho Chi Minh', 'Hoan Kiem'),
(54, '', 'ming120898@gmail.com', 'Phan Nhật Minh', '0369308670', '51 Nguyễn Nhạc', 'Hai Phong', 'Hoang Mai');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `order_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `number` int(6) NOT NULL,
  `amount` int(15) NOT NULL,
  `custom_id` int(10) NOT NULL,
  `Date` date NOT NULL,
  `ord_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`order_id`, `product_id`, `number`, `amount`, `custom_id`, `Date`, `ord_status`) VALUES
(54, 29, 1, 150000, 52, '2019-11-30', 1),
(55, 30, 1, 320000, 53, '2019-11-30', 1),
(56, 36, 1, 200000, 53, '2019-11-30', 1),
(57, 35, 1, 270000, 54, '2019-12-02', 0),
(58, 31, 3, 1260000, 54, '2019-12-02', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL,
  `product_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `product_price` int(20) NOT NULL,
  `product_image_link` varchar(250) NOT NULL,
  `product_stock` int(10) NOT NULL,
  `product_category` varchar(100) CHARACTER SET utf8 NOT NULL,
  `pro_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_price`, `product_image_link`, `product_stock`, `product_category`, `pro_details`) VALUES
(28, 'CHÂN VÁY XẾP LI CHUYỂN MÀU', 200000, '/e-shopper/assets/themes/images/ladies/5238651I472W30NM887TW1.jpg', 23, 'nữ', 'Đây là chân váy xếp li chuyển màu cho nữ'),
(29, 'ÁO THUN PHỐI MÀU SỌC THẮT DÂY', 150000, '/e-shopper/assets/themes/images/ladies/5283741RWNY51CP53Q47QG.jpg', 14, 'nữ', 'Đây là áo thun phối màu sọc cho nữ'),
(30, 'QUẦN JEAN JOGGER RÁCH', 320000, '/e-shopper/assets/themes/images/ladies/5343041U850SG1RRM6PW63.jpg', 20, 'nam', 'Đây là quần jean'),
(31, 'GIÀY  SNEAKER PHỐI MÀU CÁ TÍNH', 420000, '/e-shopper/assets/themes/images/ladies/5269902P73W500NM25KR51.jpg', 12, 'thể thao', 'Đây là giày Sneaker'),
(32, 'ÁO HOODIE TAY PHỐI NAM TÍNH', 280000, '/e-shopper/assets/themes/images/ladies/5285611F30M155CC81YH16.jpg', 16, 'nam', 'Đây là áo Hoodie nam'),
(33, 'TÚI XÁCH DU LỊCH KIM SA PARIS', 335000, '/e-shopper/assets/themes/images/ladies/5342551H41O266FE82CJ27.jpg', 17, 'nam', 'Đây là túi xách cho nữ'),
(35, 'ÁO LEN PHỐI MÀU TAY SỌC', 270000, '/e-shopper/assets/themes/images/ladies/52246816UE5GR377IH11F4.jpg', 18, 'nữ', 'Đây là áo len phối màu tay sọc'),
(36, 'ÁO THUN NAM HÌNH CON HƯƠU', 200000, '/e-shopper/assets/themes/images/ladies/2912115730AL310H34F636312.jpg', 30, 'nam', 'Đây là áo thun nam tay sọc'),
(37, 'ÁO THUN CÁ TÍNH VẢI DA', 210000, '/e-shopper/assets/themes/images/ladies/1513465637UG308C32A4344Y1.jpg', 25, 'thể thao', 'Đây là áo thun cá tính vải da cho nam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `transaction`
--

CREATE TABLE `transaction` (
  `transaction_id` int(15) NOT NULL,
  `order_id` int(10) NOT NULL,
  `custom_id` int(10) NOT NULL,
  `amount` int(15) NOT NULL,
  `created` date NOT NULL,
  `status` int(1) NOT NULL,
  `product_id` int(10) NOT NULL,
  `prod_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `prod_img_link` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `transaction`
--

INSERT INTO `transaction` (`transaction_id`, `order_id`, `custom_id`, `amount`, `created`, `status`, `product_id`, `prod_name`, `prod_img_link`) VALUES
(89, 54, 52, 150000, '2019-11-30', 0, 29, 'ÁO THUN PHỐI MÀU SỌC THẮT DÂY', '/e-shopper/assets/themes/images/ladies/5283741RWNY51CP53Q47QG.jpg'),
(90, 55, 53, 320000, '2019-11-30', 0, 30, 'QUẦN JEAN JOGGER RÁCH', '/e-shopper/assets/themes/images/ladies/5343041U850SG1RRM6PW63.jpg'),
(91, 56, 53, 200000, '2019-11-30', 0, 36, 'ÁO THUN NAM HÌNH CON HƯƠU', '/e-shopper/assets/themes/images/ladies/2912115730AL310H34F636312.jpg');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`custom_id`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Chỉ mục cho bảng `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `custom_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT cho bảng `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transaction_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
